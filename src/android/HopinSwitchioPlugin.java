package com.hopin.cordova.hopinswitchioplugin;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;

import android.os.Bundle;
import android.content.Context;
import org.apache.cordova.CallbackContext;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import android.util.Log;
import android.widget.Toast;

public class HopinSwitchioPlugin extends CordovaPlugin {

    public static Context activity;
    private static boolean isSwitchioStarted = false;
    
    public Boolean intentActionIsArmed = false;
    public String intentActionResultcode = "";
    public String intentActionTokens = "";
    public String intentActionMaskedCardNumber = "";
    
	@Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        activity = this.cordova.getActivity().getApplication();
    }
	
    public boolean execute(final String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

		// Toast.makeText(activity, action, Toast.LENGTH_SHORT).show();

        if ("startCapture".equals(action)) {
			switchioStart();
            return true;
        }
        else if ("stopCapture".equals(action)) {
			switchioStop();
            return true;
        }
        else if ("checkCapture".equals(action)) {
            
            final JSONObject results = new JSONObject();

            if(intentActionIsArmed == false){

                callbackContext.success(results);
                return true;
            }

            try {
                results.put("resultCode", intentActionResultcode)
                        .put("tokens", intentActionTokens)
                        .put("maskedCardNumber", intentActionMaskedCardNumber);
                    
            } catch (JSONException e) {
            } catch (NullPointerException e) {
            }

            callbackContext.success(results);

            intentActionIsArmed = false;

            return true;
        }
        else{

            return true;
        }
    }
	
	public void switchioStart() {
		
        if(isSwitchioStarted == true) return; // already started

		Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SENDTO);
        sendIntent.setData(Uri.parse("paya://token_v2?action=start"));
        sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(sendIntent);

        isSwitchioStarted = true;

        Toast.makeText(activity, "started", Toast.LENGTH_SHORT).show();

        IntentFilter filter = new IntentFilter();
        filter.addAction("paya.TOKEN");

        activity.registerReceiver(myBroadcastReceiver, filter);
	}
	
	public void switchioStop() {
        
        if(isSwitchioStarted == false) return; // already stopped

		Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SENDTO);
        sendIntent.setData(Uri.parse("paya://token_v2?action=stop"));
        sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(sendIntent);

        isSwitchioStarted = false;

        Toast.makeText(activity, "stopped", Toast.LENGTH_SHORT).show();
	}
	
	public BroadcastReceiver myBroadcastReceiver = new BroadcastReceiver() {

        private String intentActionType = "";
        private String intentActionData = "";
        private JSONObject intentActionDataParsed = new JSONObject();

        @Override
        public void onReceive(Context context, Intent intent) {

            intentActionType = intent.getAction();
            intentActionData = intent.getStringExtra("data");

            try {
                intentActionDataParsed = new JSONObject(intentActionData);
            } catch (JSONException e) {
                intentActionDataParsed = new JSONObject();
            }

            try {
                intentActionResultcode = intentActionDataParsed.getString("resultCode");
            } catch (JSONException e) {
                intentActionResultcode = "";
            }

            try {
                intentActionTokens = intentActionDataParsed.getString("tokens");
            } catch (JSONException e) {
                intentActionTokens = "";
            }

            try {
                intentActionMaskedCardNumber = intentActionDataParsed.getString("maskedCardNumber");
            } catch (JSONException e) {
                intentActionMaskedCardNumber = "";
            }

            intentActionIsArmed = true;

            Toast.makeText(context, "received:"+intentActionData, Toast.LENGTH_SHORT).show();
            Log.d("jjj", "received");
            Log.d("jjj", "intentActionType:"+intentActionType);
            Log.d("jjj", "intentActionResultcode:"+intentActionResultcode);
            Log.d("jjj", "intentActionTokens:"+intentActionTokens);
            Log.d("jjj", "intentActionMaskedCardNumber:"+intentActionMaskedCardNumber);
        }
    };
}

    