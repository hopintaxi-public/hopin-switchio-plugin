var HopinSwitchioPlugin = {
    interval: null,
    startCapture: function (success, failure, options) {

		cordova.exec(function(){}, function(){}, "HopinSwitchioPlugin", "startCapture", [options]);

        try{
            clearInterval(HopinSwitchioPlugin.interval);
        }
        catch(er){
            console.log("interval err");
        }

        HopinSwitchioPlugin.interval = setInterval(() => {
            
            HopinSwitchioPlugin.checkCapture(function(response){
                
                if(response.resultCode == undefined) return;
                
                success(response);
            }, 
            function(){}, 
            {});

        }, 500);
    },
    stopCapture: function (success, failure, options) {

		cordova.exec(function(){}, function(){}, "HopinSwitchioPlugin", "stopCapture", [options]);

        try{
            clearInterval(HopinSwitchioPlugin.interval);
        }
        catch(er){
            console.log("interval err");
        }
    },
    checkCapture: function (success, failure, options) {

		cordova.exec(function(data){
            console.log("checkCapture", data);
			success(data);
	    }, 
        function(){}, "HopinSwitchioPlugin", "checkCapture", [options]);
    }
};

module.exports = HopinSwitchioPlugin;
